<!DOCTYPE html>
<html>


<head>
    <meta charset="UTF-8">
    <!---<title> Responsive Registration Form | CodingLab </title>--->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/registration.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <style>
        Body {
            font-family: Calibri, Helvetica, sans-serif;
        }

        button {
            background-color: #4CAF50;
            width: 100%;
            color: orange;
            padding: 15px;
            margin: 10px 0px;
            border: none;
            cursor: pointer;
        }

        form {
            border: 415px solid #f1f1f1;
            margin-top: -23rem;
        }

        input[type=text],
        textarea,
        input[type=file],
        input[type=email],
        input[type=number],
        input[type=password] {
            width: 100%;
            margin: 8px 0;
            padding: 12px 20px;
            display: inline-block;
            border: 2px solid green;
            box-sizing: border-box;
        }

        button:hover {
            opacity: 0.7;
        }

        .cancelbtn {
            width: auto;
            padding: 10px 18px;
            margin: 10px 5px;
        }


        .container {
            padding: 25px;
            background-color: lightblue;
        }

        .error {
            color: red;
            background-color: #acf;
        }
    </style>
</head>

<body>
    <center>
        <h1>Edit Student</h1>
    </center>
    <div class="card-body">
        <form method="POST" id="signupForm" action="<?php echo base_url('update-student/' . $data['id']); ?>" class="signupForm" enctype="multipart/form-data">
            <div class="container">
                <div class="col-md-12">
                    <label for="profileImage" class="form-label">Profile Pic</label>
                    <input type="hidden" name="profile" value="<?php if (isset($data)) {
                                                                    echo $data['profileImage'];
                                                                } ?>">
                    <input type="file" class="form-control" id="profileImage" name="profileImage" accept="image/*">
                </div>
                <div class="col-md-6">
                    <img src="<?php if (isset($data) && $data['profileImage'] != null) {
                                    echo base_url() . $data['profileImage'];
                                } ?>" class="img-fluid blah" id="blah" alt="your image" width="100px" height="100px">
                </div>


                <div class="row mb-3">
                    <label for="userName" class="col-md-4 col-form-label text-md-end" name="userName"><span style="color:red">*</span>Name</label>

                    <div class="col-md-6">
                        <input id="userName" type="text" class="form-control @error('name') is-invalid @enderror" value="<?php if (isset($data)) {
                                                                                                                                echo $data['userName'];
                                                                                                                            } ?>" name="userName" required autocomplete="userName" autofocus placeholder="Enter Your Name">
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="email" class="col-md-4 col-form-label text-md-end"><span style="color:red">*</span>Email Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" value="<?php if (isset($data)) {
                                                                                                                            echo $data['email'];
                                                                                                                        } ?>" name="email" required autocomplete="email" autofocus placeholder="Enter Your Email Address">
                        <input type="hidden" id="id" name="id" value="<?php if (isset($data)) {
                                                                            echo $data['id'];
                                                                        } ?>">

                    </div>
                </div>

                <div class="row mb-3">
                    <label for="mobileNumber" class="col-md-4 col-form-label text-md-end"><span style="color:red">*</span>mobile</label>

                    <div class="col-md-6">
                        <input id="mobileNumber" type="number" class="form-control" value="<?php if (isset($data)) {
                                                                                                echo $data['mobileNumber'];
                                                                                            } ?>" name="mobileNumber" required placeholder="Enter Your mobile">

                    </div>
                </div>

                <div class="row mb-3">
                    <label for="address" class="col-md-4 col-form-label text-md-end"><span style="color:red">*</span>Address</label>

                    <div class="col-md-6">
                        <textarea name="address" id="address" class="form-control @error('mobile') is-invalid @enderror" cols="30" rows="5" required placeholder="Enter Your Address"><?php if (isset($data)) {
                                                                                                                                                                                            echo $data['address'];
                                                                                                                                                                                        } ?></textarea>
                    </div>
                </div>


                <div class="row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" id="submitBtn" name="submitBtn" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>

</html>

<script>
    $('#blah').change(function() {
        alert('1');
        const file = this.files[0];
        if (file) {
            let reader = new FileReader();
            reader.onload = function(event) {
                $('.blah').attr('src', event.target.result);
            }

            reader.readAsDataURL(file);
        }
    });
    profileImage.onchange = evt => {
        const [file] = profileImage.files
        if (file) {
            blah.src = URL.createObjectURL(file)
        }
    }
    $('.signupForm').validate({
        rules: {
            userName: {
                required: true,
            },
            email: {
                required: true,
            },
            mobileNumber: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10,
            },
            address: {
                required: true,
            }
        },
        messages: {

            userName: {
                required: "Please Enter Name",
            },
            email: {
                required: "Please Enter Email",
            },
            mobileNumber: {
                required: "Please Enter Number",
                digits: "Please enter valid phone number",
                minlength: "Phone number field accept only 10 digits",
                maxlength: "Phone number field accept only 10 digits",

            },
            address: {
                required: "Please Enter Address",
            }
        },
    });
</script>