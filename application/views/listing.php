<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="container">
        <h2>Student's List 👨‍🎓👨‍🎓</h2><br><br>
        <button class="btn-primary"> <a href="<?php echo base_url('/student-list'); ?>" class="btn-primary">Add Student</a></button><br>

        <table class="table table-bordered">
            <tr class="table-light">
                <th>S.N.</th>
                <th>profile</th>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Address</th>
                <th>Action</th>
            </tr><?PHP $sn = 1; ?>
            <?php foreach ($users as $user) : ?>
                <tr>
                    <td><?php echo $sn++ ?></td>
                    <td><img src="<?php echo $user['profileImage']; ?>" width="50px;" class="img-fluid"></td>
                    <td><?php echo $user['userName']; ?></td>
                    <td><?php echo $user['email']; ?></td>
                    <td><?php echo $user['mobileNumber']; ?></td>
                    <td><?php echo $user['address']; ?></td>
                    <td>
                        <button class="btn-primary"> <a href="<?php echo base_url('editStudent/' . $user['id']); ?>" class="btn-primary">Edit</a></button>
                        <button class="btn-danger"><a href="<?php echo base_url('delete-student/' . $user['id']); ?>" onclick="return confirm('Are you sure?')" class="btn-danger">Delete</a></button>
                        <button> <a href="<?php echo base_url('viewStudent/' . $user['id']); ?>" class="">View</a></button>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>

</body>

</html>