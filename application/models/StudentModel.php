<?php

class StudentModel extends CI_Model
{

    private $dataFile;
    private $students;

    public function __construct()
    {
        parent::__construct();
        $this->dataFile = APPPATH . 'json_files/user.json';
        // $this->dataFile = APPPATH . 'json_files/user.json';
        $this->loadData();
    }


    public function create($userData)
    {
        $users = $this->getAll();
        $maxUserId = 0;
        foreach ($users as $user) {
            if ($user['id'] > $maxUserId) {
                $maxUserId = $user['id'];
            }
        }

        $newUser = [
            'id' => $maxUserId + 1,
            'profileImage' => $userData['profileImage'],
            'userName' => $userData['userName'],
            'email' => $userData['email'],
            'mobileNumber' => $userData['mobileNumber'],
            'address' => $userData['address']
        ];
        $users[] = $newUser;
        $jsonData = json_encode($users, JSON_PRETTY_PRINT);
        file_put_contents($this->dataFile, $jsonData);

        return $newUser['id'];
    }








    public function create1($newData)
    {
        $data = $this->getAll();
        $maxUserId = 0;
        foreach ($data as $item) {
            if ($item['id'] > $maxUserId) {
                $maxUserId = $item['id'];
            }
        }
        $newData['id'] = $maxUserId + 1;
        $data[] = $newData;

        $jsonData = json_encode($data, JSON_PRETTY_PRINT);
        file_put_contents($this->dataFile, $jsonData);
        return true;
    }


    public function getAll()
    {
        return $this->students;
    }


    public function get_user_by_id($id)
    {
        $users = json_decode(file_get_contents($this->dataFile), true);

        foreach ($users as $user) {
            if ($user['id'] == $id) {
                return $user;
            }
        }

        return null; // If user with the given ID is not found
    }

    public function update($data)
    {
        $id = $data['id'];

        if (isset($this->students[$id])) {
            $this->students[$id] = $data; 
            $this->saveData();
            return true;
        }
        return false;
    }

    public function upload_image($image_data, $num, $path1)
    {
        $image = md5(date("d-m-y:h:i s")) . "_" . $num;
        if (is_array($image_data)) {
            $file_name = pathinfo(@$image_data['name'], PATHINFO_FILENAME);
            $extension = pathinfo(@$image_data['name'], PATHINFO_EXTENSION);

            if (move_uploaded_file(@$image_data['tmp_name'], $path1 . '' . $image . '.' . $extension)) {
                $image = $image . '.' . $extension;
            } else {
                $image = Null;
            }
        }
        return $image;
    }

    public function delete($id)
    {
        if (isset($this->students[$id])) {
            unset($this->students[$id]);
            $this->saveData();
            return true;
        }
        return false;
    }



    private function loadData()
    {
        $data = file_get_contents($this->dataFile);
        $this->students = json_decode($data, true);
        if (!$this->students) {
            $this->students = [];
        }
    }

    private function saveData()
    {
        file_put_contents($this->dataFile, json_encode($this->students, JSON_PRETTY_PRINT));
    }


    // public function delete($id)
    // {
    //     $data = $this->getAll();
    //     unset($data[$id]);
    //     $this->saveData($data);
    // }
}
