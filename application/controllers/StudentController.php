<?php
defined('BASEPATH') or exit('No direct script access allowed');

class StudentController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('StudentModel');
    }


    public function student_list()
    {
        $data['users'] = $this->StudentModel->getAll();
        $this->load->view('/student_list', $data);
    }
    public function listing()
    {
        $data['users'] = $this->StudentModel->getAll();
        $this->load->view('/listing', $data);
    }

    public function editStudent($id)
    {
        $data['data'] = $this->StudentModel->get_user_by_id($id);
        return  $this->load->view('/editStudent', $data);
    }

    public function viewStudent($id)
    {
        $data['view'] = $this->StudentModel->get_user_by_id($id);
        return  $this->load->view('/viewStudent', $data);
    }


    public function get_user_by_id($id)
    {
        $users = json_decode(file_get_contents(APPPATH . 'json_files/user.json'), true);

        foreach ($users as $user) {
            if ($user['id'] == $id) {
                return $user;
            }
        }

        return null;
    }

    public function insertData()
    {
        $userData = [
            'userName' => $this->input->post('userName'),
            'email' => $this->input->post('email'),
            'mobileNumber' => $this->input->post('mobileNumber'),
            'address' => $this->input->post('address')
        ];
        if (isset($_FILES['profileImage']) && $_FILES['profileImage']['name'] != "") {
            $image_data = $_FILES['profileImage'];
            $path =  'uploads/image/stusents/';
            $image = $this->StudentModel->upload_image($image_data, 1, $path);
            $userData['profileImage'] = $path . $image;
            $userId = $this->StudentModel->create($userData);
            redirect('/listing');
        } else {
            return "Image Not Found";
        }
    }
    public function showStudent($id)
    {
        $data['users'] = $this->StudentModel->getAll()[$id];
        $this->load->view('users/edit', $data);
    }

    public function updateStudent($id)
    {
        $userData = [
            'id' => $id,
            'userName' => $this->input->post('userName'),
            'email' => $this->input->post('email'),
            'mobileNumber' => $this->input->post('mobileNumber'),
            'address' => $this->input->post('address')
        ];
        if (isset($_FILES['profileImage']) && $_FILES['profileImage']['name'] != "") {
            $image_data = $_FILES['profileImage'];
            $path =  'uploads/image/stusents/';
            $image = $this->StudentModel->upload_image($image_data, 1, $path);
            $userData['profileImage'] = $path . $image;
        } else {
            $userData['profileImage'] = $this->input->post('profile');
        }
        if (!empty($userData['id'])) {
            $this->StudentModel->update($userData);
            redirect('/listing');
        } else {
            return "Not updated";
        }
    }
    public function deleteStudent($id)
    {
        $result = $this->StudentModel->delete($id);
        return redirect('/listing');
    }
}
